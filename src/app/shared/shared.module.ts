import {ListerrorsComponent} from './errors/listerrors.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    NgbDropdownModule.forRoot(),
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    HttpModule,
    RouterModule
  ],
  declarations: [
    ListerrorsComponent
  ],
  exports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ToastrModule,
    ReactiveFormsModule,
    HttpModule,
    ListerrorsComponent,
    RouterModule
  ]
})
export class SharedModule {}

