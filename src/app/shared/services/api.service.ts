import {environment} from '../../../environments/environment';
import {JwtService} from './jwt.service';
import {Injectable} from '@angular/core';
import {Http, Headers, URLSearchParams, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {map, catchError} from 'rxjs/operators';

@Injectable()
export class ApiService {

  constructor(private http: Http,
    private jwtService: JwtService) {}

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `Token ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }

  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    return this.http.get(`${environment.api_url}${path}`, {headers: this.setHeaders(), search: params})
      .pipe(catchError(this.formatErrors))
      .pipe(
        map((res: Response) => res.json())
      );
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      {headers: this.setHeaders()}
    )
      .pipe(catchError(this.formatErrors))
      .pipe(
        map((res: Response) => res.json())
      );
  }

  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded', 
                    'authorization' : 'Basic d2ViX2FwcDo=', 'accept' : 'application/json'});

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${environment.api_url}${path}`, body,
      {headers: this.headers}
    )
      .pipe(catchError(this.formatErrors))
      .pipe(
        map((res: Response) => res.json())
      );
        
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.api_url}${path}`,
      {headers: this.setHeaders()}
    )
      .pipe(catchError(this.formatErrors))
      .pipe(
        map((res: Response) => res.json())
      );
  }
}
