import {TestBed, inject} from '@angular/core/testing';
import {APP_BASE_HREF} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AuthGuard} from './auth-guard.service';
import {LoginModule} from '../../login/login.module';
import {LoginComponent} from '../../login/login.component';
import {UserService} from './user.service';
import {ApiService} from './api.service';
import {JwtService} from './jwt.service';
import { AuthService } from './auth.service';

describe('AuthGuard', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, AuthGuard, {provide: APP_BASE_HREF, useValue: '/'}, UserService, ApiService, JwtService],
      imports: [LoginModule, RouterModule.forRoot([{path: '', component: LoginComponent}])]
    });
  });

  it('should create AuthGuard instance', inject([AuthGuard], (service: AuthGuard) => {
    expect(service).toBeTruthy();
  }));
});
