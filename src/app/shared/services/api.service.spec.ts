import {TestBed, inject} from '@angular/core/testing';
import {HttpModule} from '@angular/http';
import {ApiService} from './api.service';
import {JwtService} from './jwt.service';

describe('ApiService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiService, JwtService],
      imports: [HttpModule]
    });
  });

  it('should create ApiService instance', inject([ApiService], (service: ApiService) => {
    expect(service).toBeTruthy();
  }));
});
