import {User} from '../models/user.model';
import {ApiService} from './api.service';
import {JwtService} from './jwt.service';
import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthService {
  private currentUserSubject = new BehaviorSubject<User>(new User());
  //public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(private apiService: ApiService,
    private jwtService: JwtService) {}

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  /*populate() {
    // If JWT detected, attempt to get & store user's info
    if (this.jwtService.getToken()) {
      this.apiService.post('/uaa/oauth/token', credentials)
      .pipe(map(
      data => {
        this.setAuth(data);
        return data;
      })
    )
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  }*/

  setAuth(user: User) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.access_token);
    // Set current user data into observable
    this.currentUserSubject.next(user);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next(new User());
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(credentials): Observable<User> {
    return this.apiService.post('/uaa/oauth/token', credentials)
      .pipe(map(
      data => {
        this.setAuth(data);
        return data;
      }
      ));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  testIsLogin(): boolean {
    return true;
  }

}
