import {Errors} from '../models';
import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-listerrors',
  templateUrl: './listerrors.component.html',
  styleUrls: ['./listerrors.component.css']
})
export class ListerrorsComponent {

  formattedErrors: Array<string> = [];

  @Input()
  set errors(errorList: Errors) {
    this.formattedErrors = [];

    if (errorList.errors) {
      errorList.errors.forEach(item => {
        for (const error of Object.keys(item)) {
          this.formattedErrors.push(`${item[error]}`);
        }
      });
    }
  };

  get errorList() {
    return this.formattedErrors;
  }

}
