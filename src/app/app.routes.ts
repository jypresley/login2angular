import { LoginComponent } from './login/login.component';
import { LoginRoutes } from './login/login.routes';
import {PrivateRoutes} from './private/private.routes';
import {Routes} from '@angular/router';

export const routes: Routes = [
  ...LoginRoutes,
  ...PrivateRoutes,
  {path: '**', component: LoginComponent}
];
