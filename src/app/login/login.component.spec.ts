import {TestBed, getTestBed, async, inject} from '@angular/core/testing';
import {HttpModule} from '@angular/http';
import {UserService, ApiService, JwtService} from '../shared/services';
import {AuthService} from '../shared/services/auth.service';

import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

describe('LoginComponent', () => {

  let subject: AuthService = null;
  let backend: MockBackend = null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiService, UserService, JwtService, AuthService, MockBackend],
      imports: [HttpModule]
    });
  });

  beforeEach(inject([AuthService, MockBackend], (authService: AuthService, mockBackend: MockBackend) => {
    subject = authService;
    backend = mockBackend;
  }));


  it('should create all services', inject([ApiService, UserService, AuthService], (apiService, userService, authService) => {
    expect(apiService).toBeDefined();
    expect(userService).toBeDefined();
    expect(authService).toBeDefined();
  }));

  //AuthService.attemptAuth(`username=secaadm&password=secaadmn&grant_type=password&slug=seca`).subscribe(data => expect(data.scope).toBe('openid'));

  
  it('#isLoggedIn should return false after creation', inject([UserService], (service: UserService) => {
    expect(service.isLoggedIn()).toBeFalsy();
  }));


});
