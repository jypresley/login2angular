import {Errors, User} from '../shared/models';
import {AuthService} from '../shared/services/auth.service';
import {Component} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-login-cmp',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {

  errors: Errors = new Errors();
  isSubmitting = false;
  authForm: FormGroup;
  user : User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    // use FormBuilder to create a form group
    this.authForm = this.formBuilder.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
      'slug': ['', Validators.required]
    });

  }


   onLoggedin() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let username = this.authForm.controls['username'].value;
    let password = this.authForm.controls['password'].value;;
    let slug = this.authForm.controls['slug'].value;;

    //application/x-www-form-urlencoded'
    let body = `username=${username}&password=${password}&grant_type=password&slug=${slug}`;

    //let body = `username=secaadm&password=secaadm&grant_type=password&slug=seca`;

    this.authService
      .attemptAuth(body)
      .subscribe(
      data => this.router.navigate(['/', 'dashboard']).then(nav => { 
        // true if navigation is successful
        //console.log(data.access_token); //to store in webstorage-service
        //location.href="/dashboard";
      })
      , err => {
        this.errors = err;
        this.isSubmitting = false;

        this.toastr.error("Failed to sign in! Please check your credentials and try again.")
        }
      );
  }


}

