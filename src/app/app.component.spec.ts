import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {AuthService} from './shared/services/auth.service';
import {ApiService} from './shared/services/api.service';
import {JwtService} from './shared/services/jwt.service';
import { HttpModule } from '@angular/http';

//router-outlet ignore it
import {RouterTestingModule} from '@angular/router/testing'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ApiService, AuthService, JwtService],
      declarations: [
        AppComponent
      ],
      imports: [ 
        RouterTestingModule, HttpModule
      ]
    }).compileComponents();
  }));

    
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'undefined'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual(undefined);
  }));
  
});
