// Imports
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { FormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';


// Declarations
import {AppComponent} from './app.component';
import {routes} from './app.routes';
import {LoginModule} from './login/login.module';
import {PrivateModule} from './private/private.module';
import {JwtService, ApiService, UserService, AuthGuard} from './shared/services';
import {SharedModule} from './shared/shared.module';
import {AuthService} from './shared/services/auth.service';
import {CommonModule} from '@angular/common';
import {NgbModule, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//other Declarations

// Decorator
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(routes),
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    LoginModule,
    PrivateModule
  ],
  providers: [
    {
      provide: NgbDateParserFormatter,
      useFactory: parser
    },
    ApiService, AuthService, AuthGuard, JwtService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule {
  // Module class
}

export function parser() {
  
}



