import {AuthGuard} from '../shared/services';
import {PrivateComponent} from './private.component';
import { Route} from '@angular/router';

import { DashboardRoutes } from './dashboard/dashboard.routes';

export const PrivateRoutes: Route[] = [
  {
    path: '',
    component: PrivateComponent,
    canActivate: [],
    children: [
      ...DashboardRoutes
    ]
  }
];






