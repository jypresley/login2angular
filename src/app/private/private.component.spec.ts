import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {HttpModule} from '@angular/http';
import {PrivateComponent} from './private.component';
import {UserService, ApiService, JwtService} from '../shared/services';

describe('PrivateComponent', () => {
  let component: PrivateComponent;
  let fixture: ComponentFixture<PrivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrivateComponent],
      imports: [RouterModule.forRoot([]), HttpModule],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}, UserService, ApiService, JwtService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
