import {SharedModule} from '../shared/shared.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {PrivateComponent} from './private.component';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    NgbModule,
    SharedModule,
    DashboardModule
  ],
  declarations: [PrivateComponent],
  providers: [],
  exports: [PrivateComponent]
})
export class PrivateModule {}
