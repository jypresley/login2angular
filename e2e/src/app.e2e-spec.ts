import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

//need to figure out how to traverse on angular 6...
//import { waitForAngular } from 'testcafe-angular-selectors';
//import { LoginPage } from './page-model';

describe('workspace-project Login Seca', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display login page with title LOGIN SECA PAGE', () => {
    page.navigateTo();
    //browser.pause(); //do not work npm 8.x
    //debugger;
    expect(page.getParagraphText()).toEqual('LOGIN SECA PAGE');
  });

  it('should stay on same page if button click', () => {
    page.navigateTo();
    page.getSubmitButton().click();
    expect(page.getSubmitButton()).toEqual('Sign in');
  });

  it('should be forwarded to Dashboard if correct login/password/company is typed (beta!)', () => {
    page.navigateTo();

    //need to figure out how to traverse on new angular 6...
   /* fixture `Navigate Login with Angular 6..`
    .page('/')
    .beforeEach(async t => {
        await waitForAngular();

        await t
            .typeText(loginPage.username, 'secaadm')
            .typeText(loginPage.password, 'secaadm')
            .typeText(loginPage.slug, 'seca')
            .click(loginPage.loginBtn);
    });

    expect(page.getParagraphText('p')).toEqual('dashboard');
  */

  });

});
