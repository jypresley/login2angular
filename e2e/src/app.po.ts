import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-login-cmp h1')).getText();
  }

  getSubmitButton() {
    return element(by.css('button')).getText();
  }
}
